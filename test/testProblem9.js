
const {getCheckItems,completeStateOfCheckItem}=require('../problem9.js');
const getAllCards= require('../problem5.js');

let cardChecklists = {};

getAllCards('6635a3e58eeb3009b750c7d0')
  .then((cardsData) => {
    const flattenedCards = cardsData.flat();
    const promiseArrayOfgetCheckItems = [];
    flattenedCards.forEach((card) => {
      cardChecklists[card.id] = card.idChecklists;
      console.log(cardChecklists);
      if (card.idChecklists) {
        card.idChecklists.forEach((id) => {
          promiseArrayOfgetCheckItems.push(getCheckItems(id));
        });
      }
    });
    
    return Promise.all(promiseArrayOfgetCheckItems);
  })
  .then((checkItemsData) => {
    const flattenedCheckItems = checkItemsData.flat();
    console.log(flattenedCheckItems);
    const promiseArray = [];
    flattenedCheckItems.forEach((checkItem) => {
      const cardId = Object.keys(cardChecklists).find((id) => {
        return cardChecklists[id].includes(checkItem.idChecklist);
      });
      if (cardId) {
        promiseArray.push(completeStateOfCheckItem(cardId, checkItem.id));
      }
    });
    return Promise.all(promiseArray);
  })
  .then(() => {
    console.log('all checkitems updated to complete status');
  })
  .catch((error) => {
    console.error(error);
  });


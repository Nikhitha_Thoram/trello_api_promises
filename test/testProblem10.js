
const {getCheckItems,completeStateOfCheckItem}=require('../problem10.js');
const getAllCards= require('../problem5.js');

let cardChecklists = {};

getAllCards('6635a3e58eeb3009b750c7d0')
  .then((cardsData) => {
    const flattenedCards = cardsData.flat();
    console.log(flattenedCards);
    const promiseArrayOfgetCheckItems = [];
    flattenedCards.forEach((card) => {
      cardChecklists[card.id] = card.idChecklists;
      if (card.idChecklists) {
        card.idChecklists.forEach((id) => {
          promiseArrayOfgetCheckItems.push(getCheckItems(id));
        });
      }
    });
    return Promise.all(promiseArrayOfgetCheckItems);
  })
  .then((checkItemsData) => {
    const flattenedCheckItems = checkItemsData.flat();
    console.log(flattenedCheckItems);
    const finalResult = flattenedCheckItems.reduce((promiseChain, currentCheckItem) => {

        return promiseChain.then(() => {
          const cardId = Object.keys(cardChecklists).find((cardId) => {
            return cardChecklists[cardId].includes(currentCheckItem.idChecklist);
          });
          return completeStateOfCheckItem(cardId, currentCheckItem.id);
        });
      }, Promise.resolve());
   return finalResult;
   
  })
  .then(() => {
    console.log('all checkitems updated to inComplete status sequentially');
  })
  .catch((error) => {
    console.error(error);
  });


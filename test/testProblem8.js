const deleteAllLists=require('../problem8.js');

const listIds = ['6635a1fb7c0409d41e1caa78','6635a1fb4f9c90310b9a8fc4','6635a1fbb8980ad8b5c7c8ca'];

deleteAllLists(listIds)
    .then(() => {
        console.log('All lists deleted successfully');
    })
    .catch(error => {
        console.error(error);
    });

const createBoardWithListsAndCards=require('../problem6.js');

const boardName='Daily_taks';
const listNames=['dailyLog','music','study'];
const cardNames=['doneTasks','pendingTasks','musicTime'];

createBoardWithListsAndCards(boardName,listNames,cardNames)
.then(()=>{
    console.log('board and lists and cards created sucessfully');
})
.catch((error)=>{
    console.error(error);
})


const apiKey = "6ca9c13439675ea1e194e487cd48fcfe";
const token = "ATTA1aaeb306f21934e53d31275f169172740a30580f2e7b094765dd8993923424623BB94747";

function deleteLists(listId){
    
    return fetch(`https://api.trello.com/1/lists/${listId}/closed?key=${apiKey}&token=${token}&value=true`,
        {
            method:"PUT",
        }
        )
        .then((response)=>{
           return response.json();
        })
        .catch((error)=>{
            console.error(error);
        })

}

function deleteAllLists(listIds){

    return Promise.all(listIds.map(listId=>deleteLists(listId)))
    .then((listIds)=>{
        const closedListIds=listIds.map(listId=>listId.id);
        console.log(`all lists closed simultaneously: ${closedListIds}`)
    })
    .catch(error=>{
        console.error(error);
    })
}

module.exports = deleteAllLists;
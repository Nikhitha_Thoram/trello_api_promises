const apiKey = "6ca9c13439675ea1e194e487cd48fcfe";
const token = "ATTA1aaeb306f21934e53d31275f169172740a30580f2e7b094765dd8993923424623BB94747";

function deleteListsSimultaneously(listId) {
    
    return fetch(`https://api.trello.com/1/lists/${listId}/closed?key=${apiKey}&token=${token}&value=true`,
        {
           method: "PUT",
        })
        .then((response) => {
            return response.json();
        })
        .catch(error => {
            console.error('Error fetching data',error);
        });
}

function deleteAllLists(listIds) {
    return listIds.reduce((promiseChain, listId) => {
        return promiseChain.then(() => {
            return deleteListsSimultaneously(listId)
                .then(() => {
                    console.log(`List with id: ${listId} deleted successfully`);
                });
        });
    }, Promise.resolve());
}


module.exports= deleteAllLists;
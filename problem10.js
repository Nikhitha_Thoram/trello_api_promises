
const apiKey = "6ca9c13439675ea1e194e487cd48fcfe";
const token = "ATTA1aaeb306f21934e53d31275f169172740a30580f2e7b094765dd8993923424623BB94747";

function getCheckItems(checkListId){
    return fetch(`https://api.trello.com/1/checklists/${checkListId}/checkItems?key=${apiKey}&token=${token}`,
        {
            method:'GET',
        }
        )
        .then((response)=>{
            return response.json();
        })
        .catch((error)=>{
            console.error(error);
        });
}

function completeStateOfCheckItem(cardId,checkItemId){
    return fetch(`https://api.trello.com/1/cards/${cardId}/checkItem/${checkItemId}?state=incomplete&key=${apiKey}&token=${token}`,
        {
            method:'PUT',
        }
        )
        .then((response)=>{
            return response.json();
        })
        .catch((error)=>{
            console.error(error);
        });
}

module.exports ={getCheckItems,completeStateOfCheckItem};



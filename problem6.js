
const apiKey = "6ca9c13439675ea1e194e487cd48fcfe";
const token ="ATTA1aaeb306f21934e53d31275f169172740a30580f2e7b094765dd8993923424623BB94747";

function createBoard(boardName){
    return fetch(`https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${token}`,
       {
        method:"POST",
       }
    )
    .then((response)=>{
        return response.json();
    })
    .then((data)=>{
        return data.id;
    })
    .catch(error=>{
        console.error('Error fetching data',error);
    })
}

function createLists(boardId,listName){
    
    return fetch(`https://api.trello.com/1/lists/?name=${listName}&idBoard=${boardId}&key=${apiKey}&token=${token}`,
        {
            method:'POST',
        }
        )
        .then((response)=>{
            return response.json();
        })
        .then((data)=>{
            return data.id;
        })
        .catch((error)=>{
            console.error('Error fetching data',error);
        })
}

function createCards(listId,cardName){

    return fetch(`https://api.trello.com/1/cards/?name=${cardName}&idList=${listId}&key=${apiKey}&token=${token}`,
    {
        method:'POST',
    }
    
    )
    .then((response)=>{
        return response.json();
    })
    .then((data)=>{
        return data.id;
    })
    
    .catch((error)=>{
        console.error("Error fetching data",error);
    });
}

function createBoardWithListsAndCards(boardName,listNames,cardNames){
    return createBoard(boardName)
    .then((boardId)=>{
        console.log(`board created with boardId:${boardId}`);
        const listData= listNames.map(listName=>createLists(boardId,listName))
        return Promise.all(listData);
    })
    .then((listIds)=>{
        console.log(`lists created with ids: ${listIds}`);
        let cardData = listIds.map((listId,index)=>createCards(listId,cardNames[index]));
        return Promise.all(cardData);
    })
    .then((cardIds)=>{
        console.log(`cards created with ids: ${cardIds}`)
        return cardIds;
    })
    .catch((error)=>{
        console.error(error);
    })
}

module.exports=createBoardWithListsAndCards;
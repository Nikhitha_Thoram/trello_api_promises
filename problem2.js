const apiKey = "6ca9c13439675ea1e194e487cd48fcfe";
const token ="ATTA1aaeb306f21934e53d31275f169172740a30580f2e7b094765dd8993923424623BB94747";

function createBoard(boardName) {
  return fetch(
    `https://api.trello.com/1/boards/?name=${boardName}&key=${apiKey}&token=${token}`,
    {
      method: "POST",
    }
  )
  .then((response) => {
    return response.json();
  })

  .catch((error) => {
    console.error("Error fetching data", error);
  });
}

module.exports = createBoard;
